## Bootstrap Starter Kit with Webpack, Babel, ES6, Sass

---

This starter kit is good for developers who wants to use es6 with babel compiler and webpack.

---

### Installation:

```
$ git clone git@bitbucket.org:hazar_artuner/webpack-bootstrap-starterkit.git

$ cd webpack-bootstrap-starterkit

$ npm install
```

After installation completed;

For development run:
```
$ npm run dev
```

For production export, run:
```
$ npm run prod
```

This command will export all files to dist folder with all asset dependencies.



