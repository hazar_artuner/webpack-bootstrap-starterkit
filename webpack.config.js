const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const webpack = require('webpack');

const isDevelopment = process.env.NODE_ENV === 'development';

const extractSass = new ExtractTextPlugin({
  filename: 'css/[name].[contenthash].css',
  disable: isDevelopment
});

module.exports = {
  entry: [
    './src/js/main.js',
    './src/scss/main.scss'
  ],
  output: {
    filename: 'js/[name].[chunkhash].js',
    path: __dirname + '/dist',
  },
  module: {
    rules: [
      {
        test: /.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          cacheDirectory: true,
          presets: ['env', 'es2015']
        }
      },
      {
        test: /\.css$/,
        loader: extractSass.extract({ fallback: "style-loader", use : ["css-loader"] })
      },
      {
        test: /\.scss$/,
        loader: extractSass.extract({ fallback: "style-loader", use : ["css-loader", "sass-loader"] })
      },
      {
        test: /\.less$/,
        loaders: ['style', 'css', 'less']
      },
      {
        test: /\.woff$/,
        loader: "url-loader?limit=10000&mimetype=application/font-woff&name=[path][name].[ext]"
      },
      {
        test: /\.woff2$/,
        loader: "url-loader?limit=10000&mimetype=application/font-woff2&name=[path][name].[ext]"
      },
      {
        test: /\.(svg|gif|png|jpg|jpeg)$/,
        loader: "file-loader" + (isDevelopment ? '' : "?publicPath=./&name=images/[name].[ext]")
      },
      {
        test: /\.(eot|ttf)$/,
        loader: "file-loader" + (isDevelopment ? '' : "?publicPath=./&name=fonts/[name].[ext]")
      },
      {
        test: /\.html$/,
        loader: ['html-loader']
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({template: './index.html'}),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery"
    }),
    extractSass
  ],
  resolve: {
    alias: {
      src: path.resolve(__dirname, './src/'),
    }
  }
};